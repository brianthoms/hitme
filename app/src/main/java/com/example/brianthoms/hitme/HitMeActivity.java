package com.example.brianthoms.hitme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;
import android.util.Log;

public class HitMeActivity extends AppCompatActivity {

    private Button mBtn;
    private TextView mTextView;
    String msg = "Android : ";
    private int hitCounter;
    private String bruiseLevelText, ouchMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(msg, "The onCreate() was run.");
        ouchMessage="";
        bruiseLevelText="";
        setContentView(R.layout.activity_hit_me);
        mTextView = (TextView) findViewById(R.id.hitMeTextView);

        if (savedInstanceState != null) {
            ouchMessage = savedInstanceState.getString("ouchMessage");
            hitCounter=savedInstanceState.getInt("hitCounter");
            mTextView.setText(ouchMessage);
        } else {
            hitCounter = 0;
        }
        mBtn = (Button) findViewById(R.id.hitMeButton);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitCounter++;
                if(hitCounter > 30)
                {
                    bruiseLevelText="And now my arm is about to fall off.";
                }
                else if(hitCounter > 20)
                {
                    bruiseLevelText="And now my arm is blue.";
                }
                else if(hitCounter > 10)
                {
                    bruiseLevelText="And now my arm is red.";
                }
                Toast.makeText(HitMeActivity.this, R.string.ouch,
                        Toast.LENGTH_SHORT).show();
                ouchMessage=getResources().getString(R.string.ouch) + "\nI've been hit " +
                        hitCounter + " times. " + "\n" + bruiseLevelText;
                mTextView.setText(ouchMessage);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("hitCounter", hitCounter);
        outState.putString("ouchMessage", ouchMessage);
    }
}
